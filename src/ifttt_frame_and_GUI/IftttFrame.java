package ifttt_frame_and_GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.sql.Date;
//import java.sql.Time;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 整个Ifttt窗口程序的框架类 其包含基本的主题结构逻辑
 * @author Daniel
 *
 */
public class IftttFrame extends JFrame {
	/* 菜单栏 */
	private JMenuBar jmb ;//new JMenuBar();
	/* 菜单 */
	private JMenu taskMenu, controlMenu;
	/* 菜单项 */
	private JMenuItem 	createTask,	deleteTask,	modifyTask,	allTask, 
						startTask,	pauseTask,	stopTask;
	
	/**
	 * 表示从程序运行到当前 一共新建过的任务数量<br>
	 * 亦表示这一次新建应当赋给的任务ID值
	 */
	int currentTaskIDNum = 0;

	/**
	 * 任务列表 全局 表示已创建的任务列表
	 */
	TaskDataList taskDataList;

	/**
	 * 用于显示任务列表的GUI列表类 应与任务数据结构列表类同步
	 */
	JList jlstTaskList;
	
	/**
	 * 用于支持 jlstTaskList 的 DefaultListModel
	 */
	DefaultListModel dlstmodelTaskList;
	
	
	/**
	 * 用以格式化日期时间的 SimpleDateFormat
	 */
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	
	
	/* frame界面元素声明 */
	//frame中间区域 JPanel
	JPanel jpanel_FrameCenter;
		//左侧JPanel
		JPanel jpanel_FrameUpLeft;
			//左侧上部JPanel
			JPanel jpanel_LeftUp;		
				//顶部标签 单机版IFTTT
				JLabel jlblIFTTT;
				//任务选择的 JPanel
				JPanel jpanel_TaskChooseJPanel;
				
				//JLabel 选择需要运行的任务
				JLabel jlblTaskChooseToRun;
				//组合框
				JComboBox jcboTaskChooseToRun;
			//左侧下部 任务描述 的 JPanel
			JPanel jpanel_TaskDiscriptionJPanel;
		//右侧 文本区域 JTextArea
		JTextArea jtaInstructions;
			
	//设置底部的JPanel
	JPanel jpanel_FrameDown;
		//设置 输出信息 的 JPanel
		JPanel jpanel_OutputMassage;
		
		
		
	

	/**
	 * 单个任务信息的数据结构类
	 * @author Daniel
	 *
	 */
	class TaskData {
		/**
		 * 用于标识任务的ID 从0开始
		 */
		int taskID;
		
		/**
		 * 任务名
		 */
		String taskName;
		
		/**
		 * 表示IF-THIS中的THIS条件的类型代码<br>
		 * 其与新建及修改任务界面中的组合框代码相关联<br>
		 * 在本程序中<br>
		 * thisType = 0 表示以时间为THIS触发器<br>
		 * thisType = 1 表示以指定邮箱收到邮件为THIS触发器
		 */
		int thisType;
//		java.sql.Date thisDate;
//		java.sql.Time thisTime;
		
		java.util.Date thisDateTime;
		String thisReceiveMailBox;
		String thisReceiveMailPasswd;
		
		/**
		 * 表示THEN-THAT中的THAT操作的类型代码<br>
		 * 其与新建及修改任务界面中的组合框代码相关联<br>
		 * 在本程序中<br>
		 * thatType = 0 表示以已关联的账号发微博为THAT事件<br>
		 * thatType = 1 表示以已关联的邮箱发邮件为THAT事件
		 */
		int thatType;
		String thatWeiboID;
		String thatWeiboPasswd;
		String thatSendMailBox;
		String thatSendMailTitle;
		String thatSendContent;
		
		@Override
		public String toString(){
			StringBuffer toString = new StringBuffer();
			toString = toString.append("任务ID：\t").append(taskID).append("\n");
			toString = toString.append("任务名：\t").append(taskName).append("\n");
			if(thisType == 0){
				toString = toString.append("THIS触发类型：\t按时间触发\n");
				toString = toString.append("日期时间：\t").append(dateTimeFormat.format(thisDateTime)).append("\n\n");
			}else if(thisType ==1){
				toString = toString.append("THIS触发类型：\t按收到邮件触发\n");
				toString = toString.append("接收邮箱：\t").append(thisReceiveMailBox).append("\n");
				toString = toString.append("接收邮箱密码：\t").append(thisReceiveMailPasswd).append("\n\n");
			}else{
				toString = toString.append("THIS触发器类型出错\n");
			}
			
			if(thatType == 0){
				toString = toString.append("THAT事件类型：发表微博\n");
				toString = toString.append("微博ID：\t").append(thatWeiboID).append("\n");
				toString = toString.append("微博密码：\t").append(thatWeiboPasswd).append("\n\n");
				toString = toString.append("微博内容：\n").append(thatSendContent);
			}else if(thatType ==1){
				toString = toString.append("THAT事件类型：发送邮件\n");
				toString = toString.append("目标邮箱：\t").append(thatSendMailBox).append("\n");
				toString = toString.append("邮件标题：\t").append(thatSendMailTitle).append("\n\n");
				toString = toString.append("邮件内容：\n").append(thatSendContent);
			}else{
				toString = toString.append("THAT触发器类型出错\n");
			}
			
			
			
			return toString.toString();
		}
	}
	
	/**
	 * 任务信息数据结构类TaskData 的列表<br>
	 * 即任务列表
	 * @author Daniel
	 *
	 */
	class TaskDataList extends ArrayList<TaskData>{
		
	}
		
	
	
	/* 一些可复用的GUI模版 */
		
	/**
	 * 一种信息输出的panel模版<br>
	 * 其包含一个信息描述的label 以及 一个信息输出的文本域<br>
	 * 
	 * @author Daniel
	 * 
	 */
	class HorizontalLabelTextFieldPanel extends JPanel {
		JLabel label;
		JTextField jftDetail;
		public HorizontalLabelTextFieldPanel(String jlblString, String jtfString) {
			// TODO 自动生成的构造函数存根
			setLayout(new BorderLayout());
			// 在HorizontalLabelTextFieldPanel的左部放置label
			label = new JLabel(jlblString, SwingConstants.LEFT);

			add(label, BorderLayout.WEST);

			// 输出信息的文本区域
			jftDetail = new JTextField(jtfString);

			add(jftDetail, BorderLayout.CENTER);
		}
	}
	
	/**
	 * 一种信息输出的panel模版<br>
	 * 其包含一个信息描述的label 以及 一个密码专用的文本域<br>
	 * 
	 * @author Daniel
	 * 
	 */
	class HorizontalLabelPasswdFieldPanel extends JPanel {
		JLabel label;
		JPasswordField jptDetail;
		public HorizontalLabelPasswdFieldPanel(String jlblString, String jtfString) {
			// TODO 自动生成的构造函数存根
			setLayout(new BorderLayout());
			// 在HorizontalLabelTextFieldPanel的左部放置label
			label = new JLabel(jlblString, SwingConstants.LEFT);
			
			add(label, BorderLayout.WEST);
			
			// 输出信息的文本区域
			jptDetail = new JPasswordField(jtfString);
			
			add(jptDetail, BorderLayout.CENTER);
		}
	}
		
	/**
	 * 一种信息输出的panel模版<br>
	 * 其包含一个信息描述的label 以及 一个信息输出的文本区域<br>
	 * 且该文本区域包含一个总是显示的滚动条
	 * @author Daniel
	 *
	 */
	class MessagePanel extends JPanel {
		//信息标题 JLabel
		JLabel label;
		//具体的信息内容 JTextArea
		JTextArea jtaMassage;
		//包含信息内容的 JScrollPane
		JScrollPane jscbMassage;
		
		public MessagePanel(String jlblString, String jtaString, int rows) {
			// TODO 自动生成的构造函数存根
			setLayout(new BorderLayout(0,5));
			// 在messagePanel的上部放置label
			label = new JLabel(jlblString);
			add(label, BorderLayout.NORTH);

			// 输出信息的文本区域
			jtaMassage = new JTextArea(jtaString, rows, 0);
			// jtaMassage.setBorder(new LineBorder(Color.GRAY));
			// 设置自动换行
			jtaMassage.setLineWrap(true);
			// 设置按单词换行
			jtaMassage.setWrapStyleWord(true);
			// 设置不可编辑
			jtaMassage.setEditable(false);
			
			// 对该文本区域设置滚动窗格
			jscbMassage = new JScrollPane(jtaMassage);
			// 设置总是显示滚动条
			jscbMassage.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			// 将包含滚动条的信息文本区域放置于messagePanel的中间
			add(jscbMassage, BorderLayout.CENTER);
		}
	}
	
	/**
	 * 在编辑任务界面中的THIS部分类
	 * @author Daniel
	 *
	 */
	class THIS_JPanel extends JPanel {
		/* 界面元素声明 */
		
		//JPanel THIS上部
		JPanel jpanel_THISUp;
			//JLabel THIS
			JLabel jlblTHIS;
			//THIS上部JPanel JComboBox
			JPanel jpanel_THISJComboBox;
				// JComboBox 时钟
				JComboBox jcboTime;
		
		//JPanel THIS中部
		JPanel jpanel_THISCenter;
			//JPanel THIS中部之上部
			JPanel jpanel_THISCenterUp;
				//JPanel 日期或收邮件邮箱
				JPanel jpanel_Date_ReceiveMailBOx;
				//JPanel 时间或收邮件密码
				JPanel jpanel_Time_ReceiveMailPasswd;
		
		public THIS_JPanel(){
			setLayout(new BorderLayout(10,10));
			
			//JPanel THIS上部
			jpanel_THISUp = new JPanel(new BorderLayout(10,10));
			{
				//JLabel THIS
				jlblTHIS = new JLabel("THIS",SwingConstants.LEFT);
				jlblTHIS.setFont(new Font("微软雅黑",Font.PLAIN ,18));
				
				//THIS上部JPanel JComboBox
				jpanel_THISJComboBox = new JPanel(new BorderLayout());
				{
					//JComboBox 时钟
					jcboTime = new JComboBox(new Object[]{"时钟","收到邮件"});
					
					jpanel_THISJComboBox.add(jcboTime,BorderLayout.WEST);
				}
				
				jpanel_THISUp.add(jlblTHIS,BorderLayout.NORTH);
				jpanel_THISUp.add(jpanel_THISJComboBox,BorderLayout.CENTER);
				jpanel_THISUp.add(new JPanel(),BorderLayout.WEST);
				jpanel_THISUp.add(new JPanel(),BorderLayout.EAST);
			}
			
			//JPanel THIS中部
			jpanel_THISCenter = new JPanel(new BorderLayout());
			{
				//JPanel THIS中部之上部
				jpanel_THISCenterUp = new JPanel(new GridLayout(2,0));
				{
					//JPanel 日期或收邮件邮箱
					jpanel_Date_ReceiveMailBOx = 
							new HorizontalLabelTextFieldPanel("<html>日期：&emsp;</html>", "2013-01-01");
					//JPanel 时间或收邮件密码
					jpanel_Time_ReceiveMailPasswd = 
							new HorizontalLabelPasswdFieldPanel("<html>时间：&emsp;</html>", "12:00:00");
					((HorizontalLabelPasswdFieldPanel)jpanel_Time_ReceiveMailPasswd).jptDetail.setEchoChar((char) 0);
					
					jpanel_THISCenterUp.add(jpanel_Date_ReceiveMailBOx);
					jpanel_THISCenterUp.add(jpanel_Time_ReceiveMailPasswd);
				}
				jpanel_THISCenter.add(jpanel_THISCenterUp,BorderLayout.NORTH);
			}
				
			add(jpanel_THISUp,BorderLayout.NORTH);
			add(jpanel_THISCenter,BorderLayout.CENTER);
			add(new JPanel(),BorderLayout.WEST);
			add(new JPanel(),BorderLayout.EAST);
			
			//对JPanel THIS 加框
			setBorder(new LineBorder(Color.GRAY));
		}
	}
		
	/**
	 * 在编辑任务界面中的THAT部分类
	 * @author Daniel
	 *
	 */
	class THAT_JPanel extends JPanel {
		//右侧 JPanel THAT上部
		JPanel jpanel_THATUp;
			//右侧 JLabel THAT
			JLabel jlblTHAT;
			//THat上部JPanel JComboBox
			JPanel jpanel_THATJComboBox;
				//右侧 JComboBox 任务模式
				JComboBox jcboTaskMode;
		
		//右侧 JPanel THAT中部
		JPanel jpanel_THATCenter;
			//右侧 JPanel THAT中部之上部
			JPanel jpanel_THATCenterUp;
				//右侧 JPanel ID 或 发送的目的邮箱
				JPanel jpanel_ID_DstMailBox;
				//右侧 JPanel 密码 或 发送邮件的标题
				JPanel jpanel_Passwd_MailTitle;
				
			//右侧 JPanel THAT中部之中部
			JPanel jpanel_THATCenterCenter;
			
		public THAT_JPanel() {
			setLayout(new BorderLayout(10,10));
			
			//右侧 JPanel THAT上部
			jpanel_THATUp = new JPanel(new BorderLayout(10,10));
			{
				//右侧 JLabel THAT
				jlblTHAT = new JLabel("THAT",SwingConstants.LEFT);
				jlblTHAT.setFont(new Font("微软雅黑",Font.PLAIN ,18));
				
				//THAT上部JPanel JComboBox
				jpanel_THATJComboBox = new JPanel(new BorderLayout());
				{
					//右侧 JComboBox 时钟
					jcboTaskMode = new JComboBox(new Object[]{"新浪微博","发邮件"});
					
					jpanel_THATJComboBox.add(jcboTaskMode,BorderLayout.WEST);
				}
				
				jpanel_THATUp.add(jlblTHAT,BorderLayout.NORTH);
				jpanel_THATUp.add(jpanel_THATJComboBox,BorderLayout.CENTER);
				jpanel_THATUp.add(new JPanel(),BorderLayout.WEST);
				jpanel_THATUp.add(new JPanel(),BorderLayout.EAST);
			}
			
			//右侧 JPanel THAT中部
			jpanel_THATCenter = new JPanel(new BorderLayout());
			{
				//右侧 JPanel THAT中部之上部
				jpanel_THATCenterUp = new JPanel(new GridLayout(2,0));
				{
					//右侧 JPanel ID
					jpanel_ID_DstMailBox = 
							new HorizontalLabelTextFieldPanel("<html>ID ：&emsp;&emsp;</html>", "");
					//右侧 JPanel 密码
					jpanel_Passwd_MailTitle = 
							new HorizontalLabelPasswdFieldPanel("<html>密码：&emsp;</html>", "");
					
					jpanel_THATCenterUp.add(jpanel_ID_DstMailBox);
					jpanel_THATCenterUp.add(jpanel_Passwd_MailTitle);
				}
				
				//右侧 JPanel THAT中部之中部
				//发送的内容
				jpanel_THATCenterCenter = new MessagePanel("微博内容：", "现在是12:00:00",0);
				((MessagePanel) jpanel_THATCenterCenter).jscbMassage
						.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
				((MessagePanel) jpanel_THATCenterCenter).jtaMassage.setWrapStyleWord(false);
				((MessagePanel) jpanel_THATCenterCenter).jtaMassage.setLineWrap(false);
				((MessagePanel) jpanel_THATCenterCenter).jtaMassage.setEditable(true);
				
				jpanel_THATCenter.add(jpanel_THATCenterUp,BorderLayout.NORTH);
				jpanel_THATCenter.add(jpanel_THATCenterCenter,BorderLayout.CENTER);
				jpanel_THATCenter.add(new JPanel(),BorderLayout.SOUTH);
			}
				
			add(jpanel_THATUp,BorderLayout.NORTH);
			add(jpanel_THATCenter,BorderLayout.CENTER);
			add(new JPanel(),BorderLayout.WEST);
			add(new JPanel(),BorderLayout.EAST);
			
			
			
			//对中部右侧 JPanel THAT 加框
			setBorder(new LineBorder(Color.GRAY));
		}
	}
	
	
		
	/* 各个Dialog的内部类 */
	
	/**
	 * 内部类CreateTaskDialog<br>
	 * 用于对 新建任务 对话框的GUI处理
	 * 
	 * @author Daniel
	 * 
	 */
	class CreateTaskDialog extends JDialog {
		/* Dialog界面元素声明 */
		// Dialog顶部 JPanel
		JPanel jpanel_DialogUp;
			//顶部JLabel 新建任务
			JLabel jlblNewTask;
			//顶部JPanel 任务名称
			JPanel jpanel_TaskName;
				
		// Dialog中部 JPanel
		JPanel jpanel_DialogCenter;
			//中部左侧 JPanel IF-THIS 
			JPanel jpanel_CenterLeft;
				//中部左侧 JPanel IF
				JPanel jpanel_LeftIF;
					//中部左侧 JLabel IF
					JLabel jlblLeftIF;
					
				//中部左侧 JPanel THIS
				JPanel jpanel_LeftTHIS;
				
		
			//中部右侧 JPanel THEN-THAT
			JPanel jpanel_CenterRight;
				//中部右侧 JPanel THEN
				JPanel jpanel_RightTHEN;
					//中部右侧 JLabel THEN
					JLabel jlblRightTHEN;
					
				//中部右侧 JPanel THAT
				JPanel jpanel_RightTHAT;

				
		// Dialog底部 JPanel
		JPanel jpanel_DialogDown;
			//底部JButton 确定
			JButton jbtconfirm;
			//底部JButton 重置
			JButton jbtreset;
			//底部JButton 取消
			JButton jbtcancel;
		
		/**
		 * CreateTaskDialog类的界面初始化函数<br>
		 * private类型 之能在类中调用<br> 
		 * 事实上推荐只在类的构造函数中调用
		 */
		private void createTaskDialogGUIInitialization() {
			//对CreateTaskDialog自身做一些处理
			setLayout(new BorderLayout(10,10));
			setTitle("新建任务");
			setSize(600,400);
			setLocationRelativeTo(null);
			setModalityType(ModalityType.APPLICATION_MODAL);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
			// Dialog顶部 JPanel
			jpanel_DialogUp = new JPanel(new BorderLayout());
			{
				//顶部JLabel 新建任务
				jlblNewTask = new JLabel("新建任务",SwingConstants.CENTER);
				jlblNewTask.setFont(new Font("微软雅黑",Font.PLAIN ,24));
				
				jpanel_TaskName = 
						new HorizontalLabelTextFieldPanel("<html>任务名称：&emsp;&emsp;</html>", "任务"
								+ (currentTaskIDNum + 1) + "（发微博）");
				
				jpanel_DialogUp.add(jlblNewTask,BorderLayout.NORTH);
				jpanel_DialogUp.add(jpanel_TaskName,BorderLayout.CENTER);
				jpanel_DialogUp.add(new JPanel(),BorderLayout.WEST);
				jpanel_DialogUp.add(new JPanel(),BorderLayout.EAST);
				
			}
			add(jpanel_DialogUp,BorderLayout.NORTH);
			
			
			// Dialog中部 JPanel
			jpanel_DialogCenter = new JPanel(new GridLayout(0,2));
			{
				//中部左侧 JPanel IF-THIS 
				jpanel_CenterLeft = new JPanel(new BorderLayout());
				{
					//中部左侧 JPanel IF
					jpanel_LeftIF = new JPanel(new BorderLayout());
					{	
						//中部左侧 JLabel IF
						jlblLeftIF = new JLabel("<html>&emsp;IF&emsp;</html>",SwingConstants.CENTER);
						jlblLeftIF.setFont(new Font("微软雅黑",Font.PLAIN ,18));
						
						jpanel_LeftIF.add(jlblLeftIF,BorderLayout.NORTH);
					}
					
					//中部左侧 JPanel THIS 用THIS_JPanel的实例
					jpanel_LeftTHIS = new THIS_JPanel();
	
					
					jpanel_CenterLeft.add(jpanel_LeftIF,BorderLayout.WEST);
					jpanel_CenterLeft.add(jpanel_LeftTHIS,BorderLayout.CENTER);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.EAST);
					
				}
				
				
				//中部右侧 JPanel THEN-THAT
				jpanel_CenterRight = new JPanel(new BorderLayout());
				{
					//中部右侧 JLabel THEN
					jpanel_RightTHEN = new JPanel(new BorderLayout());
					{
						//中部右侧 JLabel THEN
						jlblRightTHEN = new JLabel("<html>&emsp;THEN&emsp;</html>",SwingConstants.CENTER);
						jlblRightTHEN.setFont(new Font("微软雅黑",Font.PLAIN ,18));
						
						jpanel_RightTHEN.add(jlblRightTHEN,BorderLayout.NORTH);
					}
					
					//中部右侧 JPanel THAT 用THAT_JPanel的实例
					jpanel_RightTHAT = new THAT_JPanel();
					
				
					jpanel_CenterRight.add(jpanel_RightTHEN,BorderLayout.WEST);
					jpanel_CenterRight.add(jpanel_RightTHAT,BorderLayout.CENTER);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.EAST);
				}
				
				//将 中部左侧 JPanel IF-THIS  和 中部右侧 JPanel THEN-THAT 加入 Dialog中部 JPanel
				jpanel_DialogCenter.add(jpanel_CenterLeft);
				jpanel_DialogCenter.add(jpanel_CenterRight);
				
			}
			add(jpanel_DialogCenter,BorderLayout.CENTER);
			
			
			// Dialog底部 JPanel
			jpanel_DialogDown = new JPanel(new FlowLayout(FlowLayout.CENTER,50,5));
			{	
				//底部JButton 确定
				jbtconfirm = new JButton("确定");
				
				//底部JButton 重置
				jbtreset = new JButton("重置");
				
				//底部JButton 取消
				jbtcancel = new JButton("取消");
				
				jpanel_DialogDown.add(jbtconfirm);
				jpanel_DialogDown.add(jbtreset);
				jpanel_DialogDown.add(jbtcancel);
			}
			add(jpanel_DialogDown,BorderLayout.SOUTH);
		}
		
		/**
		 * CreateTaskDialog构造函数 <br>
		 * 实现 新建任务 对话框的界面布局
		 */
		public CreateTaskDialog(){
			/* CreateTaskDialog类的界面初始化 */
			createTaskDialogGUIInitialization();
					
			
			/* CreateTaskDialog的监听器 */
			/**
			 * CreateTaskDialog的 组合框 jcboTime 监听器 
			 */
			((THIS_JPanel)jpanel_LeftTHIS).jcboTime.addItemListener(new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					// TODO 自动生成的方法存根
					if (((THIS_JPanel) jpanel_LeftTHIS).jcboTime.getSelectedIndex() == 1) {
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.label.setText("<html>邮箱：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.label.setText("<html>密码：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.jptDetail.setText("");
						//将密码设置为 回显为'*'
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
								.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar('*');

					}
					else{
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.label.setText("<html>日期：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.jftDetail.setText("2013-01-01");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.label.setText("<html>时间：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.jptDetail.setText("12:00:00");
						//将回显重新设置为正常情况
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
								.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar((char)0);
					}
				}
			});
			
			/**
			 * CreateTaskDialog的 组合框 jcboTaskMode 监听器 
			 */
			((THAT_JPanel)jpanel_RightTHAT).jcboTaskMode.addItemListener(new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					// TODO 自动生成的方法存根
					if (((THAT_JPanel) jpanel_RightTHAT).jcboTaskMode.getSelectedIndex() == 1) {
						/* 对任务名进行重新设置 */
						String taskNameString = ((HorizontalLabelTextFieldPanel) jpanel_TaskName).jftDetail.getText();
						if(taskNameString.endsWith("（发微博）")){
							taskNameString = taskNameString.substring(0,taskNameString.lastIndexOf("（发微博）")).concat("（发邮件）");
						}else if(!taskNameString.endsWith("（发邮件）")){
							taskNameString = taskNameString.concat("（发邮件）");
						}
						((HorizontalLabelTextFieldPanel) jpanel_TaskName).jftDetail.setText(taskNameString);
						
						
						/* 对组合框进行重新设置 */
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.label.setText("<html>邮箱：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.label.setText("<html>标题：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.jptDetail.setText("");
						//将回显重新设置为正常情况
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
								.jpanel_Passwd_MailTitle).jptDetail).setEchoChar((char)0);
						
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.label.setText("邮件内容：");
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.jtaMassage.setText("现在是12:00:00");
						
					}
					else{
						/* 对任务名进行重新设置 */
						String taskNameString = ((HorizontalLabelTextFieldPanel) jpanel_TaskName).jftDetail.getText();
						if(taskNameString.endsWith("（发邮件）")){
							taskNameString = taskNameString.substring(0,taskNameString.lastIndexOf("（发邮件）")).concat("（发微博）");
						}else if(!taskNameString.endsWith("（发微博）")){
							taskNameString = taskNameString.concat("（发微博）");
						}
						((HorizontalLabelTextFieldPanel) jpanel_TaskName).jftDetail.setText(taskNameString);
						
						/* 对组合框进行重新设置 */
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.label.setText("<html>ID：&emsp;&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.label.setText("<html>密码：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.jptDetail.setText("");
						//将密码设置为 回显为'*'
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
								.jpanel_Passwd_MailTitle).jptDetail).setEchoChar('*');
						
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.label.setText("微博内容：");
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.jtaMassage.setText("现在是12:00:00");
						
						
					}
				}
			});
			
			
			/**
			 * CreateTaskDialog的 JButton 确定 监听器 
			 */
			jbtconfirm.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					/* 将当前所填写的任务信息生成一个任务对象 并将其添加到任务列表中 */
					try {
						TaskData taskData = new TaskData();
						/* 处理任务ID 任务名 */
						taskData.taskID = currentTaskIDNum;
						taskData.taskName = ((HorizontalLabelTextFieldPanel)jpanel_TaskName).jftDetail.getText();
						//对任务名的合法性进行检测 若不合法则抛出异常
						if(taskData.taskName.equals("") || taskData.taskName.equals(null))	
							throw new Exception("任务名不能为空");
							
						/* 处理THIS触发器 */
						taskData.thisType = ((THIS_JPanel)jpanel_LeftTHIS).jcboTime.getSelectedIndex();
						if(taskData.thisType == 0){
							//说明this模式是 以时间为触发器
							taskData.thisDateTime = new java.util.Date();
							 
							String dateTimeString = 
									(((HorizontalLabelTextFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
												.jftDetail.getText()) 
											+ " " 
											+ String.valueOf(((HorizontalLabelPasswdFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
													.jptDetail.getPassword());
							try {
								taskData.thisDateTime = dateTimeFormat.parse(dateTimeString);
							} catch (ParseException e1) {
								// TODO 自动生成的 catch 块
								throw new Exception("日期或时间格式不合法\n请以\"yyyy-MM-dd\"输入日期\n以\"hh:mm:ss\"输入时间");
							}
							/////////////////////////
							//此处仅完成了日期及时间的格式合法性检查
							//尚未完成日期时间的具体数值的合法性检查
							
						}else if(taskData.thisType == 1){
							//说明this模式是 以指定邮箱收到邮件为
							taskData.thisReceiveMailBox = (((HorizontalLabelTextFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
									.jftDetail.getText());
							taskData.thisReceiveMailPasswd = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
									.jptDetail.getPassword());
							
							if(taskData.thisReceiveMailBox.equals("") || taskData.thisReceiveMailBox.equals(null))	
								throw new Exception("接收邮箱不能为空");
							if(taskData.thisReceiveMailPasswd.equals("") || taskData.thisReceiveMailPasswd.equals(null))	
								throw new Exception("接收邮箱密码不能为空");
							
						}else{
							throw new Exception("THIS触发器组合框代码不合法");
						}
						
						/* 处理THAT事件 */
						taskData.thatType = ((THAT_JPanel)jpanel_RightTHAT).jcboTaskMode.getSelectedIndex();
						if(taskData.thatType == 0){
							// 说明THAT模式是 以已关联的微博账号 发送一条微博
							taskData.thatWeiboID = (((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
									.jftDetail.getText());
							taskData.thatWeiboPasswd = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_Passwd_MailTitle)
									.jptDetail.getPassword());
							taskData.thatSendContent = (((MessagePanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_THATCenterCenter)
									.jtaMassage.getText());
							
							if(taskData.thatWeiboID.equals("") || taskData.thatWeiboID.equals(null))	
								throw new Exception("微博ID不能为空");
							if(taskData.thatWeiboPasswd.equals("") || taskData.thatWeiboPasswd.equals(null))	
								throw new Exception("微博密码不能为空");
							if(taskData.thatSendContent.equals("") || taskData.thatSendContent.equals(null))	
								throw new Exception("微博内容不能为空");
							
							/////////////////////////
							//尚未进行微博ID的合法性检查

						}else if(taskData.thatType == 1){
							//说明THAT模式是 以已关联的邮箱账号 向指定邮箱发送邮件
							taskData.thatSendMailBox = (((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
									.jftDetail.getText());
							taskData.thatSendMailTitle = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_Passwd_MailTitle)
									.jptDetail.getPassword());
							taskData.thatSendContent = (((MessagePanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_THATCenterCenter)
									.jtaMassage.getText());
							
							if(taskData.thatSendMailBox.equals("") || taskData.thatSendMailBox.equals(null))	
								throw new Exception("发送目的邮箱不能为空");
							if(taskData.thatSendMailTitle.equals("") || taskData.thatSendMailTitle.equals(null))	
								throw new Exception("发送邮件标题不能为空");
							
							/////////////////////////
							//尚未进行发送邮箱的合法性检查
							
						}else {
							throw new Exception("THAT事件组合框代码不合法");
						}
						
						
						/* 将该任务添加至任务列表中 */
						taskDataList.add(taskData);
						
						/* 将该任务对应任务名添加到任务列表GUI列表类jlstTaskList中 */
						dlstmodelTaskList.addElement(taskData.taskName);
						
						jcboTaskChooseToRun.addItem(taskData.taskName);
						
						/* 当前新建过的任务数量自增 */
						currentTaskIDNum++;
						
						/* 在完成了一个任务的添加后 “关闭”新建任务对话框 */
						setVisible(false);
						dispose();
					
					} catch (Exception e1) {
						// TODO 自动生成的 catch 块
						// 对由合法性检测抛出的异常 进行处理
						// 即弹出错误信息对话框 告知用户进行合法性检查后再进行新建任务
						JOptionPane.showMessageDialog(null, e1.getMessage(),"信息不合法 请重新检查",JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});
		
			/**
			 * CreateTaskDialog的 JButton 重置 监听器 
			 */
			jbtreset.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					
					
					// 对 上部的默认新建任务名 进行重置	
					((HorizontalLabelTextFieldPanel) jpanel_TaskName).jftDetail.setText("任务" + (currentTaskIDNum + 1) + "（发微博）");
					
					// 对 THIS JPanel 进行重置
						((THIS_JPanel) jpanel_LeftTHIS).jcboTime.setSelectedIndex(0);
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.label.setText("<html>日期：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.jftDetail.setText("2013-01-01");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.label.setText("<html>时间：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.jptDetail.setText("12:00:00");
						//将回显重新设置为正常情况
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
								.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar((char)0);
					
					// 对 THAT JPanel 进行重置
						((THAT_JPanel) jpanel_RightTHAT).jcboTaskMode.setSelectedIndex(0);
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.label.setText("<html>ID：&emsp;&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.label.setText("<html>密码：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.jptDetail.setText("");
						//将密码设置为 回显为'*'
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
								.jpanel_Passwd_MailTitle).jptDetail).setEchoChar('*');
						
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.label.setText("微博内容：");
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.jtaMassage.setText("现在是12:00:00");
						
					
				}
			});
			
			/**
			 * CreateTaskDialog的 JButton 取消 监听器 
			 */
			jbtcancel.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					setVisible(false);
					dispose();
				}
			});
		
		
		
		}
	}
	
	
	/**
	 * 内部类ModifyTaskDialog<br> 
	 * 用于对修改任务 对话框的GUI处理
	 * @author Daniel
	 *
	 */
	class ModifyTaskDialog extends JDialog{
		/* Dialog界面元素声明 */
		// Dialog顶部 JPanel
		JPanel jpanel_DialogUp;
			//顶部JLabel 修改任务
			JLabel jlblModifyTask;
			//顶部JPanel 任务组合框
			JPanel jpanel_TaskComboBox;
				//顶部JLabel 选择修改任务
				JLabel jlblModifyChooseTask;
				//顶部JPanel JComboBox
				JPanel jpanel_jcboModifyChooseTask;
					//顶部JComboBox 选择修改任务
					JComboBox jcboModifyChooseTask;
				
		// Dialog中部 JPanel
		JPanel jpanel_DialogCenter;
			//中部左侧 JPanel IF-THIS 
			JPanel jpanel_CenterLeft;
				//中部左侧 JPanel IF
				JPanel jpanel_LeftIF;
					//中部左侧 JLabel IF
					JLabel jlblLeftIF;
					
				//中部左侧 JPanel THIS
				JPanel jpanel_LeftTHIS;
					
				
		
			//中部右侧 JPanel THEN-THAT
			JPanel jpanel_CenterRight;
				//中部右侧 JPanel THEN
				JPanel jpanel_RightTHEN;
					//中部右侧 JLabel THEN
					JLabel jlblRightTHEN;
					
					
				//中部右侧 JPanel THAT
				JPanel jpanel_RightTHAT;
					
				
		
		// Dialog底部 JPanel
		JPanel jpanel_DialogDown;
			//底部JButton 确定
			JButton jbtconfirm;
			//底部JButton 重置
			JButton jbtreset;
			//底部JButton 取消
			JButton jbtcancel;
			
		/**
		 * ModifyTaskDialog类的界面初始化函数<br>
		 * private类型 之能在类中调用<br> 
		 * 事实上推荐只在类的构造函数中调用
		 */
		private void modifyTaskDialogGUIInitialization(){
			//对ModifyTaskDialog自身做一些处理
			setLayout(new BorderLayout(10,10));
			setTitle("修改任务");
			setSize(600,400);
			setLocationRelativeTo(null);
			setModalityType(ModalityType.APPLICATION_MODAL);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
			// Dialog顶部 JPanel
			jpanel_DialogUp = new JPanel(new BorderLayout());
			{
				//顶部JLabel 修改任务
				jlblModifyTask = new JLabel("修改任务",SwingConstants.CENTER);
				jlblModifyTask.setFont(new Font("微软雅黑",Font.PLAIN ,24));
				
				//顶部JPanel 任务组合框
				jpanel_TaskComboBox = new JPanel(new BorderLayout());
				{
					//顶部JLabel 选择修改任务
					jlblModifyChooseTask = new JLabel("<html>任务名称：&emsp;&emsp;</html>",SwingConstants.LEFT);
					//顶部JPanel JComboBox
					jpanel_jcboModifyChooseTask = new JPanel(new BorderLayout());
					{
						//顶部JComboBox 选择修改任务
						//对jcboModifyChooseTask的初始化放在类构造函数ModifyTaskDialog()中
						jcboModifyChooseTask = new JComboBox();
						
						jpanel_jcboModifyChooseTask.add(jcboModifyChooseTask,BorderLayout.WEST);
					}
					
					jpanel_TaskComboBox.add(jlblModifyChooseTask,BorderLayout.WEST);
					jpanel_TaskComboBox.add(jpanel_jcboModifyChooseTask,BorderLayout.CENTER);
				}
				
				jpanel_DialogUp.add(jlblModifyTask,BorderLayout.NORTH);
				jpanel_DialogUp.add(jpanel_TaskComboBox,BorderLayout.CENTER);
				jpanel_DialogUp.add(new JPanel(),BorderLayout.WEST);
				jpanel_DialogUp.add(new JPanel(),BorderLayout.EAST);
				
			}
			add(jpanel_DialogUp,BorderLayout.NORTH);
			
			
			// Dialog中部 JPanel
			jpanel_DialogCenter = new JPanel(new GridLayout(0,2));
			{
				//中部左侧 JPanel IF-THIS 
				jpanel_CenterLeft = new JPanel(new BorderLayout());
				{
					//中部左侧 JPanel IF
					jpanel_LeftIF = new JPanel(new BorderLayout());
					{	
						//中部左侧 JLabel IF
						jlblLeftIF = new JLabel("<html>&emsp;IF&emsp;</html>",SwingConstants.CENTER);
						jlblLeftIF.setFont(new Font("微软雅黑",Font.PLAIN ,18));
						
						jpanel_LeftIF.add(jlblLeftIF,BorderLayout.NORTH);
					}
					
					//中部左侧 JPanel THIS 用THIS_JPanel的实例
					jpanel_LeftTHIS = new THIS_JPanel();
	
					
					jpanel_CenterLeft.add(jpanel_LeftIF,BorderLayout.WEST);
					jpanel_CenterLeft.add(jpanel_LeftTHIS,BorderLayout.CENTER);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.EAST);
					
				}
				
				
				//中部右侧 JPanel THEN-THAT
				jpanel_CenterRight = new JPanel(new BorderLayout());
				{
					//中部右侧 JLabel THEN
					jpanel_RightTHEN = new JPanel(new BorderLayout());
					{
						//中部右侧 JLabel THEN
						jlblRightTHEN = new JLabel("<html>&emsp;THEN&emsp;</html>",SwingConstants.CENTER);
						jlblRightTHEN.setFont(new Font("微软雅黑",Font.PLAIN ,18));
						
						jpanel_RightTHEN.add(jlblRightTHEN,BorderLayout.NORTH);
					}
					
					//中部右侧 JPanel THAT 用THAT_JPanel的实例
					jpanel_RightTHAT = new THAT_JPanel();
					
				
					jpanel_CenterRight.add(jpanel_RightTHEN,BorderLayout.WEST);
					jpanel_CenterRight.add(jpanel_RightTHAT,BorderLayout.CENTER);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.EAST);
				}
				
				//将 中部左侧 JPanel IF-THIS  和 中部右侧 JPanel THEN-THAT 加入 Dialog中部 JPanel
				jpanel_DialogCenter.add(jpanel_CenterLeft);
				jpanel_DialogCenter.add(jpanel_CenterRight);
				
			}
			add(jpanel_DialogCenter,BorderLayout.CENTER);
			
			
			// Dialog底部 JPanel
			jpanel_DialogDown = new JPanel(new FlowLayout(FlowLayout.CENTER,50,5));
			{	
				//底部JButton 确定
				jbtconfirm = new JButton("确定");
				
				//底部JButton 重置
				jbtreset = new JButton("重置");
				
				//底部JButton 取消
				jbtcancel = new JButton("取消");
				
				jpanel_DialogDown.add(jbtconfirm);
				jpanel_DialogDown.add(jbtreset);
				jpanel_DialogDown.add(jbtcancel);
			}
			add(jpanel_DialogDown,BorderLayout.SOUTH);
			
		}
		
		/**
		 * 用于恢复至 选择的任务修改前的状态
		 * @param indexSelectToModify
		 */
		private void resetToSelectTask(int indexSelectToModify) {
			int indexSelectTask = jcboModifyChooseTask.getSelectedIndex();
			if((indexSelectTask < 0) || (indexSelectTask >= taskDataList.size())) 
				indexSelectTask = indexSelectToModify;
			//该组合框 jcboModifyChooseTask 所选择的项目对应的 TaskData对象
			TaskData taskDataToModify = taskDataList.get(indexSelectTask);
			
			
			//根据taskDataToModify中存储的THIS触发器类型进行THIS部分的变量调整
			(((THIS_JPanel) jpanel_LeftTHIS).jcboTime).setSelectedIndex(taskDataToModify.thisType);
			if (taskDataToModify.thisType == 0) {
				//以时间为THIS触发器
				((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
						.label.setText("<html>日期：&emsp;</html>");
				((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
						.label.setText("<html>时间：&emsp;</html>");
				//处理日期时间类
				String dateTimeString = dateTimeFormat.format(taskDataToModify.thisDateTime);
				((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
						.jftDetail.setText(dateTimeString.substring(0, dateTimeString.indexOf(" ")));
				((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
						.jptDetail.setText(dateTimeString.substring(dateTimeString.indexOf(" ") + 1));
				//将回显重新设置为正常情况
				((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
						.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar((char)0);
			}
			else{
				//以指定邮箱收到邮件为THIS触发器
				((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
						.label.setText("<html>邮箱：&emsp;</html>");
				((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
						.jftDetail.setText(taskDataToModify.thisReceiveMailBox);
				((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
						.label.setText("<html>密码：&emsp;</html>");
				((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
						.jptDetail.setText(taskDataToModify.thisReceiveMailPasswd);
				//将密码设置为 回显为'*'
				((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
						.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar('*');
			}
			
			
			//根据taskDataToModify中存储的THIS触发器类型进行THIS部分的变量调整
			((THAT_JPanel) jpanel_RightTHAT).jcboTaskMode.setSelectedIndex(taskDataToModify.thatType);
			if (taskDataToModify.thatType == 0) {
				//以已关联的账号发微博为THAT事件
				((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
						.label.setText("<html>ID：&emsp;&emsp;</html>");
				((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
						.jftDetail.setText(taskDataToModify.thatWeiboID);
				((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
						.label.setText("<html>密码：&emsp;</html>");
				((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
						.jptDetail.setText(taskDataToModify.thatWeiboPasswd);
				//将密码设置为 回显为'*'
				((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
						.jpanel_Passwd_MailTitle).jptDetail).setEchoChar('*');
				
				((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
						.label.setText("微博内容：");
				((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
						.jtaMassage.setText(taskDataToModify.thatSendContent);
			}
			else{
				//以已关联的邮箱发邮件为THAT事件
				
				((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
						.label.setText("<html>邮箱：&emsp;</html>");
				((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
						.jftDetail.setText(taskDataToModify.thatSendMailBox);
				((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
						.label.setText("<html>标题：&emsp;</html>");
				((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
						.jptDetail.setText(taskDataToModify.thatSendMailTitle);
				//将回显重新设置为正常情况
				((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
						.jpanel_Passwd_MailTitle).jptDetail).setEchoChar((char)0);
				
				((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
						.label.setText("邮件内容：");
				((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
						.jtaMassage.setText(taskDataToModify.thatSendContent);
			}
		}
		
		/**
		 * ModifyTaskDialog构造函数 <br>
		 * 实现 新建任务 对话框的界面布局
		 */
		public ModifyTaskDialog(final int indexSelectToModify){
			/* ModifyTaskDialog类的界面初始化 */
			modifyTaskDialogGUIInitialization();
			
			/* 对jcboModifyChooseTask 进行初始化 */
			for(int i = 0; i < taskDataList.size(); i++){
				jcboModifyChooseTask.addItem(taskDataList.get(i).taskName.toString());
			}
			jcboModifyChooseTask.setSelectedIndex(indexSelectToModify);
			
			
			/* 根据已有taskDataList进行调整 */
			resetToSelectTask(indexSelectToModify);
	
			
			
			/* ModifyTaskDialog的监听器 */
			/**
			 * ModifyTaskDialog的 组合框 jcboModifyChooseTask 监听器 
			 */
			jcboModifyChooseTask.addItemListener(new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					// TODO 自动生成的方法存根
					resetToSelectTask(indexSelectToModify);
				}
			});
			
			
			/**
			 * ModifyTaskDialog的 组合框 jcboTime 监听器 
			 */
			((THIS_JPanel)jpanel_LeftTHIS).jcboTime.addItemListener(new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					// TODO 自动生成的方法存根
					if (((THIS_JPanel) jpanel_LeftTHIS).jcboTime.getSelectedIndex() == 1) {
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.label.setText("<html>邮箱：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.label.setText("<html>密码：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.jptDetail.setText("");
						//将密码设置为 回显为'*'
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
								.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar('*');

					}
					else{
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.label.setText("<html>日期：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
								.jftDetail.setText("2013-01-01");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.label.setText("<html>时间：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
								.jptDetail.setText("12:00:00");
						//将回显重新设置为正常情况
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THIS_JPanel) jpanel_LeftTHIS)
								.jpanel_Time_ReceiveMailPasswd).jptDetail).setEchoChar((char)0);
					}
				}
			});
			
			/**
			 * ModifyTaskDialog的 组合框 jcboTaskMode 监听器 
			 */
			((THAT_JPanel)jpanel_RightTHAT).jcboTaskMode.addItemListener(new ItemListener() {
				
				@Override
				public void itemStateChanged(ItemEvent arg0) {
					// TODO 自动生成的方法存根
					if (((THAT_JPanel) jpanel_RightTHAT).jcboTaskMode.getSelectedIndex() == 1) {
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.label.setText("<html>邮箱：&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.label.setText("<html>标题：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.jptDetail.setText("");
						//将回显重新设置为正常情况
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
								.jpanel_Passwd_MailTitle).jptDetail).setEchoChar((char)0);
						
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.label.setText("邮件内容：");
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.jtaMassage.setText("现在是12:00:00");
						
					}
					else{
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.label.setText("<html>ID：&emsp;&emsp;</html>");
						((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
								.jftDetail.setText("");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.label.setText("<html>密码：&emsp;</html>");
						((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_Passwd_MailTitle)
								.jptDetail.setText("");
						//将密码设置为 回显为'*'
						((JPasswordField) ((HorizontalLabelPasswdFieldPanel) ((THAT_JPanel) jpanel_RightTHAT)
								.jpanel_Passwd_MailTitle).jptDetail).setEchoChar('*');
						
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.label.setText("微博内容：");
						((MessagePanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_THATCenterCenter)
								.jtaMassage.setText("现在是12:00:00");
					}
				}
			});
		

			/**
			 * ModifyTaskDialog的 JButton 确定 监听器 
			 */
			jbtconfirm.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					
					
					/* 将当前所填写的任务信息生成一个任务对象 并将其添加到任务列表中 */
					try {
						//该组合框 jcboModifyChooseTask 所选择的项目对应的 TaskData对象
						int indexSelectTask = jcboModifyChooseTask.getSelectedIndex();
						if((indexSelectTask < 0) || (indexSelectTask >= taskDataList.size())) 
							indexSelectTask = indexSelectToModify;
						
						//创建一个新的TaskData对象 用以记录修改后的任务信息
						TaskData temptaskData = new TaskData();
						/* 处理任务ID 任务名 */
						temptaskData.taskID = taskDataList.get(indexSelectTask).taskID;
						temptaskData.taskName = taskDataList.get(indexSelectTask).taskName;
							
						/* 处理THIS触发器 */
						temptaskData.thisType = ((THIS_JPanel)jpanel_LeftTHIS).jcboTime.getSelectedIndex();
						if(temptaskData.thisType == 0){
							//说明this模式是 以时间为触发器
							temptaskData.thisDateTime = new java.util.Date();
							 
							String dateTimeString = 
									(((HorizontalLabelTextFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
												.jftDetail.getText()) 
											+ " " 
											+ String.valueOf(((HorizontalLabelPasswdFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
													.jptDetail.getPassword());
							try {
								temptaskData.thisDateTime = dateTimeFormat.parse(dateTimeString);
							} catch (ParseException e1) {
								// TODO 自动生成的 catch 块
								throw new Exception("日期或时间格式不合法\n请以\"yyyy-MM-dd\"输入日期\n以\"hh:mm:ss\"输入时间");
							}
							/////////////////////////
							//此处仅完成了日期及时间的格式合法性检查
							//尚未完成日期时间的具体数值的合法性检查
							
						}else if(temptaskData.thisType == 1){
							//说明this模式是 以指定邮箱收到邮件为
							temptaskData.thisReceiveMailBox = (((HorizontalLabelTextFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Date_ReceiveMailBOx)
									.jftDetail.getText());
							temptaskData.thisReceiveMailPasswd = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THIS_JPanel)jpanel_LeftTHIS).jpanel_Time_ReceiveMailPasswd)
									.jptDetail.getPassword());
							
							if(temptaskData.thisReceiveMailBox.equals("") || temptaskData.thisReceiveMailBox.equals(null))	
								throw new Exception("接收邮箱不能为空");
							if(temptaskData.thisReceiveMailPasswd.equals("") || temptaskData.thisReceiveMailPasswd.equals(null))	
								throw new Exception("接收邮箱密码不能为空");
							
						}else{
							throw new Exception("THIS触发器组合框代码不合法");
						}
						
						/* 处理THAT事件 */
						temptaskData.thatType = ((THAT_JPanel)jpanel_RightTHAT).jcboTaskMode.getSelectedIndex();
						if(temptaskData.thatType == 0){
							// 说明THAT模式是 以已关联的微博账号 发送一条微博
							temptaskData.thatWeiboID = (((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
									.jftDetail.getText());
							temptaskData.thatWeiboPasswd = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_Passwd_MailTitle)
									.jptDetail.getPassword());
							temptaskData.thatSendContent = (((MessagePanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_THATCenterCenter)
									.jtaMassage.getText());
							
							if(temptaskData.thatWeiboID.equals("") || temptaskData.thatWeiboID.equals(null))	
								throw new Exception("微博ID不能为空");
							if(temptaskData.thatWeiboPasswd.equals("") || temptaskData.thatWeiboPasswd.equals(null))	
								throw new Exception("微博密码不能为空");
							if(temptaskData.thatSendContent.equals("") || temptaskData.thatSendContent.equals(null))	
								throw new Exception("微博内容不能为空");
							
							/////////////////////////
							//尚未进行微博ID的合法性检查

						}else if(temptaskData.thatType == 1){
							//说明THAT模式是 以已关联的邮箱账号 向指定邮箱发送邮件
							temptaskData.thatSendMailBox = (((HorizontalLabelTextFieldPanel) ((THAT_JPanel) jpanel_RightTHAT).jpanel_ID_DstMailBox)
									.jftDetail.getText());
							temptaskData.thatSendMailTitle = String.valueOf(((HorizontalLabelPasswdFieldPanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_Passwd_MailTitle)
									.jptDetail.getPassword());
							temptaskData.thatSendContent = (((MessagePanel)((THAT_JPanel)jpanel_RightTHAT).jpanel_THATCenterCenter)
									.jtaMassage.getText());
							
							if(temptaskData.thatSendMailBox.equals("") || temptaskData.thatSendMailBox.equals(null))	
								throw new Exception("发送目的邮箱不能为空");
							if(temptaskData.thatSendMailTitle.equals("") || temptaskData.thatSendMailTitle.equals(null))	
								throw new Exception("发送邮件标题不能为空");
							
							/////////////////////////
							//尚未进行发送邮箱的合法性检查
							
						}else {
							throw new Exception("THAT事件组合框代码不合法");
						}
						
						/* 将该任务添加至任务列表中 */
						taskDataList.set(indexSelectTask,temptaskData);
						
						/* 在完成了一个任务的添加后 “关闭”新建任务对话框 */
						setVisible(false);
						dispose();
						
					} catch (Exception e1) {
						// TODO 自动生成的 catch 块
						// 对由合法性检测抛出的异常 进行处理
						// 即弹出错误信息对话框 告知用户进行合法性检查后再进行新建任务
						JOptionPane.showMessageDialog(null, e1.getMessage(),"信息不合法 请重新检查",JOptionPane.INFORMATION_MESSAGE);
					}
					
				}
			});
		
			/**
			 * ModifyTaskDialog的 JButton 重置 监听器 
			 */
			jbtreset.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					resetToSelectTask(indexSelectToModify);
				}
			});
			
			/**
			 * ModifyTaskDialog的 JButton 取消 监听器 
			 */
			jbtcancel.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					setVisible(false);
					dispose();
				}
			});
		}
	}
	
	
	/**
	 * 内部类DeleteTaskDialog<br> 
	 * 用于对 删除任务 对话框的GUI处理
	 * @author Daniel
	 *
	 */
	class DeleteTaskDialog extends JDialog{
		/* Dialog界面元素声明 */
		// Dialog顶部 JLabel 查看、删除任务
		JLabel jlblAllDeleteTask;
				
		// Dialog中部 JPanel
		JPanel jpanel_DialogCenter;
			//中部左侧 JPanel 整个任务列表 
			JPanel jpanel_CenterLeft;
				//中部左侧 JPanel JLable 任务列表
				JPanel jpanel_jlblTaskList;
					//中部左侧 JLabel 任务列表
					JLabel jlblTaskList;
					
				//中部左侧 JScrollPane 任务列表
				JScrollPane jscb_jlstTaskList;	
					//jlstTaskList已在frame中定义
					//此处仅需将其添加即可
				
			//中部右侧 JPanel 任务信息总JPanel
			JPanel jpanel_CenterRight;
				//中部右侧 JPanel 选中任务详细信息
				JPanel jpanel_TaskDetailMessage;
				
		// Dialog底部 JPanel
		JPanel jpanel_DialogDown;
			//底部JButton 删除
			JButton jbtdelete;
			//底部JButton 退出
			JButton jbtquit;
			
		
		/**
		 * DeleteTaskDialog类的界面初始化函数<br>
		 * private类型 之能在类中调用<br> 
		 * 事实上推荐只在类的构造函数中调用
		 */
		private void deleteTaskDialogGUIInitialization(int indexSelectToDelete){
			//对DeleteTaskDialog自身做一些处理
			setLayout(new BorderLayout(10,10));
			setTitle("删除任务");
			setSize(550,350);
			setLocationRelativeTo(null);
			setModalityType(ModalityType.APPLICATION_MODAL);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
			// Dialog顶部 JLabel 查看、删除任务
			jlblAllDeleteTask = new JLabel("查看、删除任务",SwingConstants.CENTER);
			{
				jlblAllDeleteTask.setFont(new Font("微软雅黑",Font.PLAIN ,24));
			}
			add(jlblAllDeleteTask,BorderLayout.NORTH);
			
			
			// Dialog中部 JPanel
			jpanel_DialogCenter = new JPanel(new GridLayout(0,2,10,10));
			{
				//中部左侧 JPanel 整个任务列表 
				jpanel_CenterLeft = new JPanel(new BorderLayout(0,10));
				{
					//中部左侧 JLabel 任务列表
					jlblTaskList = new JLabel("<html>&emsp;任务列表：</html>",SwingConstants.LEFT);
					
					
					//中部左侧 JScrollPane 任务列表
					jscb_jlstTaskList = new JScrollPane(jlstTaskList);
					jscb_jlstTaskList.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
						//jlstTaskList已在frame中定义
						//此处仅需将其添加即可
						
					
					jpanel_CenterLeft.add(jlblTaskList,BorderLayout.NORTH);
					jpanel_CenterLeft.add(jscb_jlstTaskList,BorderLayout.CENTER);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.WEST);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.EAST);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.SOUTH);
				}
				
				//中部右侧 JPanel 任务信息总JPanel
				jpanel_CenterRight = new JPanel(new BorderLayout(0,10));
				{
					//中部右侧 JPanel 选中任务详细信息
					jpanel_TaskDetailMessage = new MessagePanel("选中任务的详细信息：", "被选中任务的详细信息", 0);
					((MessagePanel)jpanel_TaskDetailMessage).setLayout(new BorderLayout(0,10));
					((MessagePanel)jpanel_TaskDetailMessage).add(((MessagePanel)jpanel_TaskDetailMessage).label,BorderLayout.NORTH);
					((MessagePanel)jpanel_TaskDetailMessage).add(((MessagePanel)jpanel_TaskDetailMessage).jscbMassage,BorderLayout.CENTER);
					
					jpanel_CenterRight.add(jpanel_TaskDetailMessage,BorderLayout.CENTER);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.WEST);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.EAST);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.SOUTH);
					
				}
				
				//将 中部左侧 JPanel 整个任务列表   和   中部右侧 JPanel 选中任务详细信息 加入 Dialog中部 JPanel
				jpanel_DialogCenter.add(jpanel_CenterLeft);
				jpanel_DialogCenter.add(jpanel_CenterRight);
			}
			add(jpanel_DialogCenter,BorderLayout.CENTER);
			
			
			// Dialog底部 JPanel
			jpanel_DialogDown = new JPanel(new FlowLayout(FlowLayout.CENTER,50,5));
			{	
				//底部JButton 删除
				jbtdelete = new JButton("删除");
				
				//底部JButton 退出
				jbtquit = new JButton("退出");
				
				jpanel_DialogDown.add(jbtdelete);
				jpanel_DialogDown.add(jbtquit);
			}
			add(jpanel_DialogDown,BorderLayout.SOUTH);
			
		}
	
		
		public DeleteTaskDialog(int indexSelectToDelete){
			/* DeleteTaskDialog类的界面初始化 */
			deleteTaskDialogGUIInitialization(indexSelectToDelete);
			
			//需要将jlstTaskList 默认选中设为被删除的项
			jlstTaskList.setSelectedIndex(indexSelectToDelete);
			if	(indexSelectToDelete >= 0)
				((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText(taskDataList.get(indexSelectToDelete).toString());
			else{
				//保证在被删除最后一个任务时不会 去读taskDataList数组 而是现实默认信息
				((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText("被选中任务的详细信息");
			}
			
			
			/* DeleteTaskDialog的监听器*/
			
			/**
			 * DeleteTaskDialog类中对jlstTaskList的监听器
			 */
			jlstTaskList.addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					// TODO 自动生成的方法存根
					int indexOfTask = (jlstTaskList.getSelectedIndex());
					if	(indexOfTask >= 0)
						((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText(taskDataList.get(indexOfTask).toString());
					else{
						//保证在被删除最后一个任务时不会 去读taskDataList数组 而是现实默认信息
						((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText("被选中任务的详细信息");
						
					}
				}
			});
			
			
			/**
			 * DeleteTaskDialog的 JButton 删除 监听器 
			 */
			jbtdelete.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					int indexOfTaskToDelete = jlstTaskList.getSelectedIndex();
					taskDataList.remove(indexOfTaskToDelete);
					dlstmodelTaskList.remove(indexOfTaskToDelete);
					jcboTaskChooseToRun.removeItemAt(indexOfTaskToDelete);
					System.out.println(taskDataList.size());
				}
			});
			
			/**
			 * DeleteTaskDialog的 JButton 退出 监听器 
			 */
			jbtquit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					setVisible(false);
					dispose();
				}
			});
		}
	}

	
	/**
	 * 内部类AllTaskDialog<br> 
	 * 用于对 查看所有任务 对话框的GUI处理
	 * @author Daniel
	 *
	 */
	class AllTaskDialog extends JDialog{
		/* Dialog界面元素声明 */
		// Dialog顶部 JLabel 查看、删除任务
		JLabel jlblAllDeleteTask;
				
		// Dialog中部 JPanel
		JPanel jpanel_DialogCenter;
			//中部左侧 JPanel 整个任务列表 
			JPanel jpanel_CenterLeft;
				//中部左侧 JPanel JLable 任务列表
				JPanel jpanel_jlblTaskList;
					//中部左侧 JLabel 任务列表
					JLabel jlblTaskList;
					
				//中部左侧 JScrollPane 任务列表
				JScrollPane jscb_jlstTaskList;	
					//jlstTaskList已在frame中定义
					//此处仅需将其添加即可
				
			//中部右侧 JPanel 任务信息总JPanel
			JPanel jpanel_CenterRight;
				//中部右侧 JPanel 选中任务详细信息
				JPanel jpanel_TaskDetailMessage;
				
		// Dialog底部 JPanel
		JPanel jpanel_DialogDown;
			//底部JButton 删除
			JButton jbtdelete;
			//底部JButton 退出
			JButton jbtquit;
			
		
		/**
		 * AllTaskDialog类的界面初始化函数<br>
		 * private类型 之能在类中调用<br> 
		 * 事实上推荐只在类的构造函数中调用
		 */
		private void allTaskDialogGUIInitialization(){
			//对AllTaskDialog自身做一些处理
			setLayout(new BorderLayout(10,10));
			setTitle("查看任务");
			setSize(550,350);
			setLocationRelativeTo(null);
			setModalityType(ModalityType.APPLICATION_MODAL);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			
			// Dialog顶部 JLabel 查看、删除任务
			jlblAllDeleteTask = new JLabel("查看、删除任务",SwingConstants.CENTER);
			{
				jlblAllDeleteTask.setFont(new Font("微软雅黑",Font.PLAIN ,24));
			}
			add(jlblAllDeleteTask,BorderLayout.NORTH);
			
			
			// Dialog中部 JPanel
			jpanel_DialogCenter = new JPanel(new GridLayout(0,2,10,10));
			{
				//中部左侧 JPanel 整个任务列表 
				jpanel_CenterLeft = new JPanel(new BorderLayout(0,10));
				{
					//中部左侧 JLabel 任务列表
					jlblTaskList = new JLabel("<html>&emsp;任务列表：</html>",SwingConstants.LEFT);
					
					//中部左侧 JScrollPane 任务列表
					jscb_jlstTaskList = new JScrollPane(jlstTaskList);
					jscb_jlstTaskList.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
						//jlstTaskList已在frame中定义
						//此处仅需将其添加即可
						//但此处需要将JList 默认取消选择
						jlstTaskList.clearSelection();
						
					
					jpanel_CenterLeft.add(jlblTaskList,BorderLayout.NORTH);
					jpanel_CenterLeft.add(jscb_jlstTaskList,BorderLayout.CENTER);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.WEST);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.EAST);
					jpanel_CenterLeft.add(new JPanel(),BorderLayout.SOUTH);
				}
				
				//中部右侧 JPanel 任务信息总JPanel
				jpanel_CenterRight = new JPanel(new BorderLayout(0,10));
				{
					//中部右侧 JPanel 选中任务详细信息
					jpanel_TaskDetailMessage = new MessagePanel("选中任务的详细信息：", "被选中任务的详细信息", 0);
					((MessagePanel)jpanel_TaskDetailMessage).setLayout(new BorderLayout(0,10));
					((MessagePanel)jpanel_TaskDetailMessage).add(((MessagePanel)jpanel_TaskDetailMessage).label,BorderLayout.NORTH);
					((MessagePanel)jpanel_TaskDetailMessage).add(((MessagePanel)jpanel_TaskDetailMessage).jscbMassage,BorderLayout.CENTER);
					
					jpanel_CenterRight.add(jpanel_TaskDetailMessage,BorderLayout.CENTER);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.WEST);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.EAST);
					jpanel_CenterRight.add(new JPanel(),BorderLayout.SOUTH);
					
				}
				
				//将 中部左侧 JPanel 整个任务列表   和   中部右侧 JPanel 选中任务详细信息 加入 Dialog中部 JPanel
				jpanel_DialogCenter.add(jpanel_CenterLeft);
				jpanel_DialogCenter.add(jpanel_CenterRight);
			}
			add(jpanel_DialogCenter,BorderLayout.CENTER);
			
			
			// Dialog底部 JPanel
			jpanel_DialogDown = new JPanel(new FlowLayout(FlowLayout.CENTER,50,5));
			{	
				//底部JButton 删除
				jbtdelete = new JButton("删除");
				
				//底部JButton 退出
				jbtquit = new JButton("退出");
				
				jpanel_DialogDown.add(jbtdelete);
				jpanel_DialogDown.add(jbtquit);
			}
			add(jpanel_DialogDown,BorderLayout.SOUTH);
			
		}
	
		
		public AllTaskDialog(){
			/* AllTaskDialog类的界面初始化 */
			allTaskDialogGUIInitialization();
			
			
			/* AllTaskDialog的监听器*/
			
			/**
			 * AllTaskDialog类中对jlstTaskList的监听器
			 */
			jlstTaskList.addListSelectionListener(new ListSelectionListener() {
				
				@Override
				public void valueChanged(ListSelectionEvent arg0) {
					// TODO 自动生成的方法存根
					int indexOfTask = (jlstTaskList.getSelectedIndex());
					if	(indexOfTask >= 0)
						((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText(taskDataList.get(indexOfTask).toString());
					else{
						//保证在被删除最后一个任务时不会 去读taskDataList数组 而是现实默认信息
						((MessagePanel)jpanel_TaskDetailMessage).jtaMassage.setText("被选中任务的详细信息");
						
					}
				}
			});
			
			
			/**
			 * AllTaskDialog的 JButton 删除 监听器 
			 */
			jbtdelete.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					int indexOfTaskToDelete = jlstTaskList.getSelectedIndex();
					taskDataList.remove(indexOfTaskToDelete);
					dlstmodelTaskList.remove(indexOfTaskToDelete);
					jcboTaskChooseToRun.removeItemAt(indexOfTaskToDelete);
					System.out.println(taskDataList.size());
				}
			});
			
			/**
			 * AllTaskDialog的 JButton 退出 监听器 
			 */
			jbtquit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO 自动生成的方法存根
					setVisible(false);
					dispose();
				}
			});
		
		}
	
	}
	
	
	
	/**
	 * 整个Frame框架 IftttFrame类的界面初始化函数<br>
	 * private类型 之能在类中调用<br> 
	 * 事实上推荐只在类的构造函数中调用
	 */
	private void iftttFrameGUIInitialization() {
		/* 设置菜单栏 */
		jmb = new JMenuBar();
		taskMenu = new JMenu("任务");
		controlMenu = new JMenu("控制");
		
		//任务菜单taskMenu中的 菜单项
		createTask 	= new JMenuItem("新建任务");
		modifyTask 	= new JMenuItem("修改任务");
		deleteTask 	= new JMenuItem("删除任务");
		allTask 	= new JMenuItem("查看所有任务");
		
		startTask 	= new JMenuItem("开始任务");
		pauseTask 	= new JMenuItem("暂停任务");
		stopTask 	= new JMenuItem("结束任务");
		
		//将菜单项 添加到菜单栏中
		jmb.add(taskMenu);
		jmb.add(controlMenu);
		setJMenuBar(jmb);
		taskMenu.add(createTask);
		taskMenu.add(modifyTask);
		taskMenu.add(deleteTask);
		taskMenu.add(allTask);
		
		controlMenu.add(startTask);
		controlMenu.add(pauseTask);
		controlMenu.add(stopTask);
		
		/* 设置frame框架的布局 */
		setLayout(new BorderLayout(5,5));
		
		//frame中间区域的JPanel设置
		jpanel_FrameCenter = new JPanel(new GridLayout(0,2,10,10));
		{	
			//左侧JPanel设置
			jpanel_FrameUpLeft = new JPanel(new BorderLayout());
			{
				jpanel_LeftUp = new JPanel(new GridLayout(2,0,5,5));
				{
					//顶部标签 单机版IFTTT
					jlblIFTTT = new JLabel("单机版IFTTT",SwingConstants.CENTER);
					jlblIFTTT.setFont(new Font("微软雅黑",Font.PLAIN ,26));
					
					//设置任务选择的 JPanel
					jpanel_TaskChooseJPanel = new JPanel(new GridLayout(0,2));
					{
						//设置JLabel 选择需要运行的任务
						jlblTaskChooseToRun = new JLabel("选择需要运行的任务：");
						
						//设置组合框
						jcboTaskChooseToRun = new JComboBox();
						
						
						jpanel_TaskChooseJPanel.add(jlblTaskChooseToRun);
						jpanel_TaskChooseJPanel.add(jcboTaskChooseToRun);
					}
					
					jpanel_LeftUp.add(jlblIFTTT);
					jpanel_LeftUp.add(jpanel_TaskChooseJPanel);
				}
				
				//设置 任务描述 的 JPanel
				jpanel_TaskDiscriptionJPanel = 
						new MessagePanel("正在运行的任务描述：", "当前任务的详细描述",0);
				
				jpanel_FrameUpLeft.add(jpanel_LeftUp,BorderLayout.NORTH);
				jpanel_FrameUpLeft.add(jpanel_TaskDiscriptionJPanel,BorderLayout.CENTER);
			}
			
			//右侧文本区域 使用说明 设置
			String instructions = "使用说明：\n"
					.concat("1.首先用户新建任务\n")
					.concat("2.然后在选择控制->运行任务来开始任务\n")
					.concat("3.可在下列文本域内查看当前任务的完整描述");
			jtaInstructions = new JTextArea(instructions);
			// 设置边框
			jtaInstructions.setBorder(new LineBorder(Color.GRAY));
			// 设置自动换行
			jtaInstructions.setLineWrap(true);
			// 设置按单词换行
			jtaInstructions.setWrapStyleWord(true);
			// 设置不可编辑
			jtaInstructions.setEditable(false);
			
			
			jpanel_FrameCenter.add(jpanel_FrameUpLeft);
			jpanel_FrameCenter.add(jtaInstructions);
		}
		add(jpanel_FrameCenter,BorderLayout.CENTER);
		
		//设置底部的JPanel
		jpanel_FrameDown = new JPanel(new BorderLayout(5,5));
		{
			//设置 输出信息 的 JPanel
			jpanel_OutputMassage = 
					new MessagePanel("输出信息：", "当前任务的执行情况...（实时的监控当前正在运行的任务，并实时的输出必要的信息）",5);
			add(jpanel_OutputMassage,BorderLayout.SOUTH);
			jpanel_FrameDown.add(jpanel_OutputMassage,BorderLayout.CENTER);
			//将左右都留出一定空隙
			jpanel_FrameDown.add(new JPanel(),BorderLayout.WEST);
			jpanel_FrameDown.add(new JPanel(),BorderLayout.EAST);
		}	
		add(jpanel_FrameDown,BorderLayout.SOUTH);
		
		//将顶部及左右都留出一定空隙
		add(new JPanel(),BorderLayout.WEST);
		add(new JPanel(),BorderLayout.EAST);
		add(new JPanel(),BorderLayout.NORTH);
		
	}
	
	/**
	 * IftttFrame框架的构造函数<br>
	 * 包含框架的监听器
	 */
	public IftttFrame() {
		// TODO 自动生成的构造函数存根
		
		/* 创建任务列表GUI列表类的对象 */
		dlstmodelTaskList = new DefaultListModel();
		jlstTaskList = new JList(dlstmodelTaskList);
		jlstTaskList.setBorder(new LineBorder(Color.GRAY));
		jlstTaskList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		/* 创建任务信息列表对象 */
		taskDataList = new TaskDataList();
		
		/* 整个Frame框架 IftttFrame类的界面初始化 */
		iftttFrameGUIInitialization();
		
		/* 监听器 */
		
		/* 菜单栏监听器 */
		
		/* 任务菜单 */
		//新建任务
		createTask.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO 自动生成的方法存根
				CreateTaskDialog createTaskDialog = new CreateTaskDialog();
				createTaskDialog.setVisible(true);
			}
		});
		//修改任务
		modifyTask.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO 自动生成的方法存根
				ModifyTaskDialog modifyTaskDialog = new ModifyTaskDialog(jcboTaskChooseToRun.getSelectedIndex());
				modifyTaskDialog.setVisible(true);
			}
		});
		
		
		
		//删除任务
		deleteTask.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO 自动生成的方法存根
				DeleteTaskDialog deleteTaskDialog = new DeleteTaskDialog(jcboTaskChooseToRun.getSelectedIndex());
				deleteTaskDialog.setVisible(true);
			}
		});
		//查看所有任务
		allTask.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO 自动生成的方法存根
				AllTaskDialog allTaskDialog = new AllTaskDialog();
				allTaskDialog.setVisible(true);
			}
		});
		
		
		
	}

	
	
	
	
	public static void main(String[] args){
		JFrame testIfttt = new IftttFrame();
		testIfttt.setTitle("单机版 IFTTT");
		testIfttt.setSize(600, 400);
		testIfttt.setLocationRelativeTo(null);
		testIfttt.setDefaultCloseOperation(EXIT_ON_CLOSE);
		testIfttt.setVisible(true);
	}
}
